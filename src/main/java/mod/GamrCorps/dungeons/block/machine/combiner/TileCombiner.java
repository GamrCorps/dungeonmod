package mod.GamrCorps.dungeons.block.machine.combiner;

import mod.GamrCorps.dungeons.recipe.combiner.CombinerRecipes;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import java.util.Random;

public class TileCombiner extends TileEntity implements ISidedInventory {
    private int[] inputSlots = {0, 1, 2, 3};
    private int[] outputSlots = {4, 5, 6, 7, 8};
    private int[] fuelSlots = {9};
    private ItemStack[] combinerContents = new ItemStack[10];
    private Random random = new Random();
    protected String customName;
    String owner = "";
    public static final String publicName = "tileEntityQuarry";
    String name = "tileEntityQuarry";
    int numPlayersUsing = 0;

    public String getName() {
        return name;
    }

    @Override
    public String getInventoryName() {
        return "Pressure Combiner";
    }

    @Override
    public void readFromNBT(NBTTagCompound nbt) {
        super.readFromNBT(nbt);
        NBTTagList nbttaglist = nbt.getTagList("Items", 10);
        this.combinerContents = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i) {
            NBTTagCompound nbttagcompound1 = nbttaglist.getCompoundTagAt(i);
            int j = nbttagcompound1.getByte("Slot") & 255;

            if (j >= 0 && j < this.combinerContents.length) {
                this.combinerContents[j] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }

        if (nbt.hasKey("CustomName", 8)) {
            this.customName = nbt.getString("CustomName");
        }
        readCustomNBT(nbt);
    }

    @Override
    public void writeToNBT(NBTTagCompound nbt) {
        super.writeToNBT(nbt);
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.combinerContents.length; ++i) {
            if (this.combinerContents[i] != null) {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte) i);
                this.combinerContents[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        nbt.setTag("Items", nbttaglist);

        if (this.hasCustomInventoryName()) {
            nbt.setString("CustomName", this.customName);
        }
        writeCustomNBT(nbt);
    }

    public void readCustomNBT(NBTTagCompound nbt) {
        //null
    }

    public void writeCustomNBT(NBTTagCompound nbt) {
        nbt.setString("CustomName", "Combiner");
    }

    @Override
    public ItemStack getStackInSlotOnClosing(int slot) {
        if (this.combinerContents[slot] != null) {
            ItemStack itemstack = this.combinerContents[slot];
            this.combinerContents[slot] = null;
            return itemstack;
        } else {
            return null;
        }
    }

    @Override
    public void setInventorySlotContents(int slot, ItemStack itemstack) {
        this.combinerContents[slot] = itemstack;

        if (itemstack != null && itemstack.stackSize > this.getInventoryStackLimit()) {
            itemstack.stackSize = this.getInventoryStackLimit();
        }

        this.markDirty();
    }

    @Override
    public ItemStack getStackInSlot(int slot) {
        return this.combinerContents[slot];
    }

    @Override
    public ItemStack decrStackSize(int slot, int amount) {
        if (this.combinerContents[slot] != null) {
            ItemStack itemstack;

            if (this.combinerContents[slot].stackSize <= amount) {
                itemstack = this.combinerContents[slot];
                this.combinerContents[slot] = null;
                this.markDirty();
                return itemstack;
            } else {
                itemstack = this.combinerContents[slot].splitStack(amount);

                if (this.combinerContents[slot].stackSize == 0) {
                    this.combinerContents[slot] = null;
                }

                this.markDirty();
                return itemstack;
            }
        } else {
            return null;
        }
    }

    public void openInventory() {
        if (this.numPlayersUsing < 0) {
            this.numPlayersUsing = 0;
        }

        ++this.numPlayersUsing;
        this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.numPlayersUsing);
        this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, this.getBlockType());
        this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord - 1, this.zCoord, this.getBlockType());
    }

    public void closeInventory() {
        if (this.getBlockType() instanceof BlockCombiner) {
            --this.numPlayersUsing;
            this.worldObj.addBlockEvent(this.xCoord, this.yCoord, this.zCoord, this.getBlockType(), 1, this.numPlayersUsing);
            this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord, this.zCoord, this.getBlockType());
            this.worldObj.notifyBlocksOfNeighborChange(this.xCoord, this.yCoord - 1, this.zCoord, this.getBlockType());
        }
    }

    //FIX!!!
    public boolean isItemValidForSlot(int slot, ItemStack itemstack) {
        if (CombinerRecipes.isItemValidForSlot(itemstack.getItem()) && slot >= 0 && slot < 4) {
            return true;
        }
        return false;
    }

    public boolean hasCustomInventoryName() {
        return this.customName != null && this.customName.length() > 0;
    }

    public int getInventoryStackLimit() {
        return 64;
    }

    public int getSizeInventory() {
        return 10;
    }

    public boolean isUseableByPlayer(EntityPlayer p_70300_1_) {
        return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : p_70300_1_.getDistanceSq((double) this.xCoord + 0.5D, (double) this.yCoord + 0.5D, (double) this.zCoord + 0.5D) <= 64.0D;
    }

    public int[] getAccessibleSlotsFromSide(int p_94128_1_) {
        return p_94128_1_ == 0 ? outputSlots : (p_94128_1_ == 1 ? inputSlots : fuelSlots);
    }

    //FIX!!!
    public boolean canInsertItem(int slot, ItemStack itemstack, int side) {
        return this.isItemValidForSlot(slot, itemstack);
    }

    public boolean canExtractItem(int slot, ItemStack itemstack, int side) {
        return itemstack.getItem() == Items.bucket || side == 0 || (slot >= 0 && slot < 4);
    }
}