package mod.GamrCorps.dungeons.block.machine.combiner;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockCombiner extends BlockContainer {

    public BlockCombiner(){
        super(Material.iron);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int meta){
        return new TileCombiner();
    }
}
