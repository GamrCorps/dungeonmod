package mod.GamrCorps.dungeons.block.towerdungeon.building;

import cpw.mods.fml.common.registry.GameRegistry;
import mod.GamrCorps.dungeons.main.DungeonMod;
import net.minecraft.block.Block;
import org.apache.logging.log4j.Level;

import java.util.Random;

public class TowerDungeonBuildingBlocks {
    public static Random random;

    public static Block towerDungeonWallBlueUnbreakable = new BlockTowerWallBlue(random, true);
    public static String towerDungeonWallBlueUnbreakableName = "TowerDungeonWallUnbreakableBlue";

    public static Block towerDungeonWallBlue = new BlockTowerWallBlue(random, false);
    public static String towerDungeonWallBlueName = "TowerDungeonWallBlue";

    public static void init(){
        DungeonMod.logger.log(Level.INFO, "Registering Tower Dungeon Building Blocks");
        GameRegistry.registerBlock(towerDungeonWallBlueUnbreakable, towerDungeonWallBlueUnbreakableName);
        GameRegistry.registerBlock(towerDungeonWallBlue, towerDungeonWallBlueName);
    }
}
