package mod.GamrCorps.dungeons.block.towerdungeon.building;

import mod.GamrCorps.dungeons.block.commondungeon.base.IDungeonWall;
import mod.GamrCorps.dungeons.world.gen.TowerDungeonGenerator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

import java.util.Random;

public class BlockTowerWallBlue extends IDungeonWall {
    Random randomB;

    public BlockTowerWallBlue(Random randomA, boolean unbreakable) {
        super("BlockTowerWallBlue", unbreakable);
        this.randomB = randomA;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        Random random = new Random();
        TowerDungeonGenerator.generate1(world, random, x, y, z);
        return true;
    }
}
