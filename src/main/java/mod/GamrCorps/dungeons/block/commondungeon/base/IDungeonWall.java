package mod.GamrCorps.dungeons.block.commondungeon.base;

import mod.GamrCorps.dungeons.block.towerdungeon.building.TowerDungeonBuildingBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class IDungeonWall extends Block {

    public IDungeonWall(String name, boolean unbreakable) {
        super(Material.rock);
        this.setCreativeTab(CreativeTabs.tabDecorations);
        this.setBlockName(name);
        if (unbreakable) {
            this.setBlockUnbreakable();
        } else {
            this.setHardness(1.5F);
            this.setResistance(10.0F);
        }
    }

    public static void init() {
        TowerDungeonBuildingBlocks.init();
    }
}
