package mod.GamrCorps.dungeons.main;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = DungeonMod.MODID, version = DungeonMod.VERSION, name = DungeonMod.NAME)
public class DungeonMod {
    public static final String MODID = "extremedungeons";
    public static final String VERSION = "0.1";
    public static final String NAME = "Extreme Dungeons";

    public static final Logger logger = LogManager.getLogger("Extreme Dungeons");

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        DungeonMod.logger.log(Level.INFO,"Entering Pre-Initialization Phase");

        MainRegistry.blockInit();

        MainRegistry.itemInit();

        DungeonMod.logger.log(Level.INFO,"Exiting Pre-Initialization Phase");
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        DungeonMod.logger.log(Level.INFO,"Entering Pre-Initialization Phase");

        //null

        DungeonMod.logger.log(Level.INFO,"Exiting Pre-Initialization Phase");
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        DungeonMod.logger.log(Level.INFO,"Entering Pre-Initialization Phase");

        //null

        DungeonMod.logger.log(Level.INFO,"Exiting Pre-Initialization Phase");
    }
}
