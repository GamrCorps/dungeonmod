package mod.GamrCorps.dungeons.main;

import mod.GamrCorps.dungeons.block.towerdungeon.building.TowerDungeonBuildingBlocks;
import mod.GamrCorps.dungeons.item.loot.LootItems;
import org.apache.logging.log4j.Level;

public class MainRegistry {
    public static void blockInit(){
        DungeonMod.logger.log(Level.INFO,"Registering Blocks");
        //IDungeonWall.init();
        TowerDungeonBuildingBlocks.init();
    }

    public static void itemInit(){
        DungeonMod.logger.log(Level.INFO,"Registering Items");
        LootItems.init();
    }
}
