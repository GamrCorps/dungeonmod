package mod.GamrCorps.dungeons.recipe;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class ModRecipeItem {
    public static Item item;
    public static int amount;

    public ModRecipeItem(Item itemA, int amountA){
        item = itemA;
        amount = amountA;
    }

    public ModRecipeItem(Block blockA, int amountA){
        item = Item.getItemFromBlock(blockA);
        amount = amountA;
    }

    public static Item getItem(){
        return item;
    }

    public static int getAmount(){
        return amount;
    }
}
