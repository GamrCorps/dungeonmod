package mod.GamrCorps.dungeons.recipe;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

public class ModOutputItem {
    public static Item item;
    public static int amount;
    public static int chance;

    public ModOutputItem(Item itemA, int amountA, int chanceA){
        item = itemA;
        amount = amountA;
        chance = chanceA;
    }

    public ModOutputItem(Block blockA, int amountA, int chanceA){
        item = Item.getItemFromBlock(blockA);
        amount = amountA;
        chance = chanceA;
    }
}
