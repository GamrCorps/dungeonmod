package mod.GamrCorps.dungeons.recipe.combiner;

import mod.GamrCorps.dungeons.recipe.ModOutputItem;
import mod.GamrCorps.dungeons.recipe.ModRecipeItem;
import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.List;

public class CombinerRecipe {
    public static List<ModRecipeItem> inputs = new ArrayList<ModRecipeItem>();
    public static List<ModOutputItem> outputs = new ArrayList<ModOutputItem>();

    public CombinerRecipe(ModRecipeItem input1, ModRecipeItem input2, ModRecipeItem input3, ModRecipeItem input4, ModOutputItem output1, ModOutputItem output2, ModOutputItem output3, ModOutputItem output4, ModOutputItem output5, ModOutputItem output6, ModOutputItem output7, ModOutputItem output8, ModOutputItem output9, ModOutputItem output10) {
        inputs.add(input1);
        inputs.add(input2);
        if (input3 != null) {
            inputs.add(input3);
        }
        if (input4 != null) {
            inputs.add(input4);
        }
        if (output1 != null) {
            outputs.add(output1);
        }
        if (output2 != null) {
            outputs.add(output2);
        }
        if (output3 != null) {
            outputs.add(output3);
        }
        if (output4 != null) {
            outputs.add(output4);
        }
        if (output5 != null) {
            outputs.add(output5);
        }
        if (output6 != null) {
            outputs.add(output6);
        }
        if (output7 != null) {
            outputs.add(output7);
        }
        if (output8 != null) {
            outputs.add(output8);
        }
        if (output9 != null) {
            outputs.add(output9);
        }
        if (output10 != null) {
            outputs.add(output10);
        }
    }

    public static boolean isItemInput(Item item){
        for (int i = 0; i <4;i++) {
            if (inputs.get(i).getItem() == item) {
                return true;
            }
        }
        return false;
    }
}