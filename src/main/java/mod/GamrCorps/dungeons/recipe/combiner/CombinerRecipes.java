package mod.GamrCorps.dungeons.recipe.combiner;

import mod.GamrCorps.dungeons.recipe.ModOutputItem;
import mod.GamrCorps.dungeons.recipe.ModRecipeItem;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.List;

public class CombinerRecipes {
    public static List<CombinerRecipe> registerCombinerRecipes() {
        List<CombinerRecipe> recipes = new ArrayList<CombinerRecipe>();
        recipes.add(new CombinerRecipe(new ModRecipeItem(Items.iron_ingot, 2), new ModRecipeItem(Items.coal, 1), null, null, new ModOutputItem(Items.iron_chestplate, 1, 100), null, null, null, null, null, null, null, null, null));
        return recipes;
    }

    public static List<CombinerRecipe> getRecipes() {
        List<CombinerRecipe> recipes = registerCombinerRecipes();
        return recipes;
    }

    public static boolean isItemValidForSlot(Item item){
        List<CombinerRecipe> recipes = getRecipes();
        for (int i = 0; i < recipes.size(); i++){
            if (recipes.get(i).isItemInput(item)){
                return true;
            }
        }
        return false;
    }
}