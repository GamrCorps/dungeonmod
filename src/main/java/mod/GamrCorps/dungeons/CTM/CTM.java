package mod.GamrCorps.dungeons.CTM;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class CTM {

    /*
        @SideOnly(Side.CLIENT)
        public IIcon getIcon(int Aside, int aMeta)
        {
            return Aside == 1 ? this.field_149935_N : (Aside == 0 ? this.field_149935_N : (Aside != Ameta ? this.blockIcon : this.field_149936_O));
        }

        @SideOnly(Side.CLIENT)
        public void registerBlockIcons(IIconRegister iIconRegister)
        {

            this.blockIcon = p_149651_1_.registerIcon("furnace_side");
            this.field_149936_O = p_149651_1_.registerIcon(this.field_149932_b ? "furnace_front_on" : "furnace_front_off");
            this.field_149935_N = p_149651_1_.registerIcon("furnace_top");

            this.finalIIcons[0] = iIconRegister.registerIcon(iIcons[0]);

        }
    */

    /*
    icons[] = {
    all borders,1
    no top,2
    no bottom,3
    no left,4
    no right,5
    no top left,6
    no top right,7
    no top bottom,8
    no left right,9
    no left bottom,10
    no right bottom,11
    no top left right,12
    no top left bottom,13
    no top right bottom,14
    no left right bottom,15
    no borders,16
    }
     */


    public static IIcon getTopIIcon(World w, int i, int j, int k, Block b, String[] icons, IIconRegister iconRegister) {
        //Sides {+x, -x, +z, -z}
        boolean sides[] = {false, false, false, false};

        if (w.getBlock(i + 1, j, k + 1) == b) {
            sides[0] = true;
        }
        if (w.getBlock(i + 1, j, k - 1) == b) {
            sides[1] = true;
        }
        if (w.getBlock(i - 1, j, k + 1) == b) {
            sides[2] = true;
        }
        if (w.getBlock(i - 1, j, k - 1) == b) {
            sides[3] = true;
        }
        /*
        [ ][2][ ]
        [0][b][1]
        [ ][3][ ]
         */
        if (sides[0]) {
            if (sides[1]) {
                if (sides[2]) {
                    if (sides[3]) {
                        //no borders
                        return iconRegister.registerIcon(icons[16]);
                    } else {
                        //no top left right
                        return iconRegister.registerIcon(icons[12]);
                    }
                } else {
                    if (sides[3]) {
                        //no left right bottom
                        return iconRegister.registerIcon(icons[15]);
                    } else {
                        //no left right
                        return iconRegister.registerIcon(icons[9]);
                    }
                }
            } else {
                if (sides[2]) {
                    if (sides[3]) {
                        //no top left bottom
                        return iconRegister.registerIcon(icons[13]);
                    } else {
                        //no top left
                        return iconRegister.registerIcon(icons[6]);
                    }
                } else {
                    if (sides[3]) {
                        //no left bottom
                        return iconRegister.registerIcon(icons[10]);
                    } else {
                        //no left
                        return iconRegister.registerIcon(icons[4]);
                    }
                }
            }
        }
        /*
        [ ][2][ ]
        [0][b][1]
        [ ][3][ ]
         */
        else {//there is a 0
            if (sides[1]) {
                if (sides[2]) {
                    if (sides[3]) {
                        //no top right bottom
                        return iconRegister.registerIcon(icons[14]);
                    } else {
                        //no top right
                        return iconRegister.registerIcon(icons[7]);
                    }
                } else {
                    if (sides[3]) {
                        //no right bottom
                        return iconRegister.registerIcon(icons[11]);
                    } else {
                        //no right
                        return iconRegister.registerIcon(icons[5]);
                    }
                }
            } else {
                if (sides[2]) {
                    if (sides[3]) {
                        //no top bottom
                        return iconRegister.registerIcon(icons[8]);
                    } else {
                        //no top
                        return iconRegister.registerIcon(icons[2]);
                    }
                } else {
                    if (sides[3]) {
                        //no bottom
                        return iconRegister.registerIcon(icons[3]);
                    } else {
                        //all borders
                        return iconRegister.registerIcon(icons[1]);
                    }
                }
            }
        }
    }
}
