package mod.GamrCorps.dungeons.item.loot;

import mod.GamrCorps.dungeons.main.BetaTesters;
import mod.GamrCorps.dungeons.main.DungeonMod;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityBat;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;

import java.util.List;

public class ItemBatKiller extends Item {
    public ItemBatKiller() {
        super();
        this.maxStackSize = 1;
        this.setCreativeTab(CreativeTabs.tabTools);
        this.setUnlocalizedName("BatKiller");
    }

    @Override
    public void addInformation(ItemStack itemstack, EntityPlayer player, List list, boolean bool) {
        if (itemstack.stackTagCompound == null) {
            itemstack.stackTagCompound = new NBTTagCompound();
        }
        list.add(EnumChatFormatting.GREEN + "[DUNGEON LOOT]");
        for (String ign : BetaTesters.getList()) {
            if (player.getCommandSenderName().equals(ign)) {
                //1020matthew
                list.add(EnumChatFormatting.BOLD + "" + EnumChatFormatting.GOLD + "" + EnumChatFormatting.BOLD + "BETA TESTER BONUS!");
                list.add("Range: 10" + EnumChatFormatting.GOLD + "(+5)" + EnumChatFormatting.GRAY + " blocks");
                itemstack.stackTagCompound.setInteger("Range", 15);
                return;
            }
        }
        list.add("Range: 10 blocks");
        itemstack.stackTagCompound.setInteger("Range", 10);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemstack, World world, EntityPlayer player) {
        DungeonMod.logger.log(Level.INFO, "Killing Bats...");
        double range = itemstack.stackTagCompound.getInteger("Range");
        List list = world.getEntitiesWithinAABB(EntityBat.class, AxisAlignedBB.getBoundingBox(player.posX - range, player.posY - range, player.posZ - range, player.posX + range, player.posY + range, player.posZ + range));
        if (list.size() != 0) {
            for (int i = 0; i < list.size(); ++i) {
                DungeonMod.logger.log(Level.INFO, list.get(i));
                Entity entity = (Entity) list.get(i);
                if (entity instanceof EntityBat) {
                    ((EntityBat) entity).setHealth(0.0F);
                }
            }
        }
        return itemstack;
    }
}