package mod.GamrCorps.dungeons.item.loot;

import net.minecraft.block.Block;
import net.minecraft.item.Item;

import java.util.Random;

public class DungeonChestLoot {
    public Item lootItem;
    public int lootChance;
    public int minAmount;
    public int maxAmount;

    public DungeonChestLoot(Item item, int chance, int minStackSize, int maxStackSize) {
        this.lootItem = item;
        this.lootChance = chance;
        this.minAmount = minStackSize;
        this.maxAmount = maxStackSize;
    }

    public DungeonChestLoot(Block block, int chance, int minStackSize, int maxStackSize) {
        this.lootItem = Item.getItemFromBlock(block);
        this.lootChance = chance;
        this.minAmount = minStackSize;
        this.maxAmount = maxStackSize;
    }

    public Item getItem() {
        return this.lootItem;
    }

    public int getChance() {
        return this.lootChance;
    }

    public int getStackMin() {
        return this.minAmount;
    }

    public int getStackMax() {
        return this.maxAmount;
    }

    public int getRandomStackSize() {
        Random random = new Random();
        return random.nextInt(maxAmount) + minAmount;
    }
}
