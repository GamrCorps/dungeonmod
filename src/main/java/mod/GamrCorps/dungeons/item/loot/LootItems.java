package mod.GamrCorps.dungeons.item.loot;

import cpw.mods.fml.common.registry.GameRegistry;
import mod.GamrCorps.dungeons.main.DungeonMod;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LootItems {
    public static Item lootBatKiller = new ItemBatKiller();
    public static List<DungeonChestLoot> dungeonLoot = new ArrayList<DungeonChestLoot>();

    public static DungeonChestLoot saddle = new DungeonChestLoot(Items.saddle, 10, 1, 1);
    public static DungeonChestLoot iron_ingot = new DungeonChestLoot(Items.iron_ingot, 10, 1, 4);
    public static DungeonChestLoot bread = new DungeonChestLoot(Items.bread, 10, 1, 1);
    public static DungeonChestLoot wheat = new DungeonChestLoot(Items.wheat, 10, 1, 4);
    public static DungeonChestLoot gunpowder = new DungeonChestLoot(Items.gunpowder, 10, 1, 4);
    public static DungeonChestLoot string = new DungeonChestLoot(Items.string, 10, 1, 4);
    public static DungeonChestLoot bucket = new DungeonChestLoot(Items.bucket, 10, 1, 1);
    public static DungeonChestLoot golden_apple = new DungeonChestLoot(Items.golden_apple, 1, 1, 1);
    public static DungeonChestLoot redstone = new DungeonChestLoot(Items.redstone, 10, 1, 4);
    public static DungeonChestLoot record_13 = new DungeonChestLoot(Items.record_13, 4, 1, 1);
    public static DungeonChestLoot record_cat = new DungeonChestLoot(Items.record_cat, 4, 1, 1);
    public static DungeonChestLoot name_tag = new DungeonChestLoot(Items.name_tag, 10, 1, 1);
    public static DungeonChestLoot iron_horse_armor = new DungeonChestLoot(Items.iron_horse_armor, 5, 1, 1);
    public static DungeonChestLoot golden_horse_armor = new DungeonChestLoot(Items.golden_horse_armor, 2, 1, 1);
    public static DungeonChestLoot diamond_horse_armor = new DungeonChestLoot(Items.diamond_horse_armor, 1, 1, 1);
    public static DungeonChestLoot batKiller = new DungeonChestLoot(LootItems.lootBatKiller, 10, 1, 1);

    public static void init() {
        DungeonMod.logger.log(Level.INFO, "Registering Dungeon Loot Items");
        GameRegistry.registerItem(lootBatKiller, "LootBatKiller");
        registerDungeonLoot();
    }

    public static void registerDungeonLoot() {
        DungeonMod.logger.log(Level.INFO, "Registering Dungeon Chest Loot");
        dungeonLoot.add(saddle);
        dungeonLoot.add(iron_ingot);
        dungeonLoot.add(bread);
        dungeonLoot.add(wheat);
        dungeonLoot.add(gunpowder);
        dungeonLoot.add(string);
        dungeonLoot.add(bucket);
        dungeonLoot.add(golden_apple);
        dungeonLoot.add(redstone);
        dungeonLoot.add(record_13);
        dungeonLoot.add(record_cat);
        dungeonLoot.add(name_tag);
        dungeonLoot.add(iron_horse_armor);
        dungeonLoot.add(golden_horse_armor);
        dungeonLoot.add(diamond_horse_armor);
        dungeonLoot.add(batKiller);
    }

    public static ItemStack getRandomLoot() {
        Random random = new Random();
        List<Item> temp = new ArrayList<Item>();
        for (int i = 0; i < dungeonLoot.size(); i++) {
            for (int j = 0; j <= dungeonLoot.get(i).getChance(); j++) {
                temp.add(dungeonLoot.get(i).getItem());
                //DungeonMod.logger.log(Level.INFO, dungeonLoot.get(i).getItem());
            }
        }
        Item item = temp.get(random.nextInt(temp.size()));//java.lang.IllegalArgumentException: n must be positive
        return new ItemStack(item, getStackSizeLootFromItem(item), 0);
    }

    public static int getStackSizeLootFromItem(Item item) {
        DungeonChestLoot dungeonChestLoot = new DungeonChestLoot(Items.saddle,10,1,1);
        int size;
        for (int i = 0; i < dungeonLoot.size(); i++) {
            if (item == dungeonLoot.get(i).getItem()) {
                dungeonChestLoot = dungeonLoot.get(i);
            }
        }
        size = dungeonChestLoot.getRandomStackSize();
        return size;
    }

}