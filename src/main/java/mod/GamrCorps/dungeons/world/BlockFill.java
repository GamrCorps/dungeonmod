package mod.GamrCorps.dungeons.world;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class BlockFill {
    public static boolean fillRectangle(World world, int x1, int y1, int z1, int x2, int y2, int z2, Block block) {
        int mnx = Math.min(x1, x2);
        int mxx = Math.max(x1, x2);
        int mny = Math.min(y1, y2);
        int mxy = Math.max(y1, y2);
        int mnz = Math.min(z1, z2);
        int mxz = Math.max(z1, z2);

        for (int x = mnx; x <= mxx; x++) {
            for (int y = mny; y <= mxy; y++) {
                for (int z = mnz; z <= mxz; z++){
                    world.setBlock(x,y,z,block);
                }
            }
        }

        return true;
    }
}
