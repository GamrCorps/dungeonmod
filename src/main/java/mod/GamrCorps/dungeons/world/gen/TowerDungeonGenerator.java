package mod.GamrCorps.dungeons.world.gen;

import mod.GamrCorps.dungeons.main.DungeonMod;
import mod.GamrCorps.dungeons.world.gen.tower.TowerFloors;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TowerDungeonGenerator extends WorldGenerator {
    public boolean generate(World world, Random randomA, int x, int y, int z) {
        Random random = new Random();
        int floorAmount = random.nextInt(20 - 7 + 1) + 7;
        List<String> floors = new ArrayList<String>();
        List<Integer> floorHeights = new ArrayList<Integer>();
        for (int i = 0; i <= floorAmount; i++) {
            String floorName = TowerFloors.randomFloor(random);
            floors.add(TowerFloors.genFloor(floorName, x, y + (i * 6), z, world, "blue"));
            floorHeights.add(TowerFloors.getFloorHeight(floors.get(i - 1)));
            DungeonMod.logger.log(Level.INFO, "Generated Floor \"" + floorName + "\"(" + floorHeights.get(i).toString() + ")");
        }
        return true;
    }

    public static boolean generate1(World world, Random randomA, int x, int y, int z) {
        //int floorAmount = random.nextInt(20 - 7 + 1) + 7;
        Random random = new Random();
        int floorAmount = random.nextInt(11) + 3;
        List<String> floors = new ArrayList<String>();
        List<Integer> floorHeights = new ArrayList<Integer>();
        for (int i = 0; i <= floorAmount; i++) {
            if (i == 0) {
                floors.add("Entrance");
                TowerFloors.genFloor_entrance(world, x, y, z, "blue");
                floorHeights.add(6);
                DungeonMod.logger.log(Level.INFO, "Generated Floor \"" + "Entrance" + "\"(" + "6" + ")");
            } else {
                String floorName = TowerFloors.randomFloor(random);
                floors.add(TowerFloors.genFloor(floorName, x, y + (i * 6), z, world, "blue"));
                floorHeights.add(TowerFloors.getFloorHeight(floors.get(i)));
                DungeonMod.logger.log(Level.INFO, "Generated Floor \"" + floorName + "\"(" + floorHeights.get(i).toString() + ")");
            }
        }
        return true;
    }
}