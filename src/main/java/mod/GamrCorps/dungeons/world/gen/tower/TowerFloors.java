package mod.GamrCorps.dungeons.world.gen.tower;

import mod.GamrCorps.dungeons.block.towerdungeon.building.TowerDungeonBuildingBlocks;
import mod.GamrCorps.dungeons.item.loot.LootItems;
import mod.GamrCorps.dungeons.main.DungeonMod;
import mod.GamrCorps.dungeons.world.BlockFill;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.world.World;
import org.apache.logging.log4j.Level;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TowerFloors {

    public static String[] floors = {
            "Base",
            "Loot",
            "Entrance",
    };
    public static int[] floorWeights = {
            1,
            3,
            0,
    };

    public static String randomFloor(Random random) {
        List<String> temp = new ArrayList<String>();
        for (int i = 0; i < floors.length; i++) {
            for (int j = 0; j < floorWeights[i]; j++) {
                temp.add(floors[i]);
            }
        }
        Random randomA = new Random();
        //return temp.get(random.nextInt(temp.toArray().length) - 1);
        return temp.get(randomA.nextInt(temp.size()));
    }

    public static int getFloorHeight(String floor) {
        for (String iFloor : floors) {
            if (iFloor.equals(floor)) {
                return 6;
            }
        }
        return 6;
    }

    public static String genFloor(String floorName, int x, int y, int z, World worldA, String color) {
        boolean done;
        boolean success = false;
        try {
            Class[] cArg = new Class[5];
            cArg[0] = World.class;
            cArg[1] = int.class;
            cArg[2] = int.class;
            cArg[3] = int.class;
            cArg[4] = String.class;
            Method m = TowerFloors.class.getMethod("genFloor_" + floorName.toLowerCase(), cArg);
            try {
                done = (Boolean) m.invoke(m, worldA, x, y, z, color);
                success = done;
            } catch (Exception e) {
                DungeonMod.logger.log(Level.ERROR, "Error in generating tower floor \"" + floorName + "\"(A:" + e + "), generating \"Base\" floor instead.");
                done = genFloor_base(worldA, x, y, z, color);
            }
        } catch (Exception e) {
            DungeonMod.logger.log(Level.ERROR, "Error in generating tower floor \"" + floorName + "\"(B:" + e + "), generating \"Base\" floor instead.");
            done = genFloor_base(worldA, x, y, z, color);
        }
        //done = genFloor_loot(worldA, x, y, z, color);
        //success = done;
        return done ? success ? floorName : "Base" : null;
    }

    public static boolean genFloor_base(World world, int i, int j, int k, String color) {
        if (color.toLowerCase().equals("blue")) {
            //Floor
            BlockFill.fillRectangle(world, i - 10, j, k - 10, i + 10, j, k + 10, TowerDungeonBuildingBlocks.towerDungeonWallBlueUnbreakable);
            //Roof
            BlockFill.fillRectangle(world, i - 10, j + 5, k - 10, i + 10, j + 5, k + 10, TowerDungeonBuildingBlocks.towerDungeonWallBlueUnbreakable);
            //++Wall
            BlockFill.fillRectangle(world, i + 10, j + 1, k + 10, i + 10, j + 4, k - 10, TowerDungeonBuildingBlocks.towerDungeonWallBlueUnbreakable);
            //--Wall
            BlockFill.fillRectangle(world, i - 10, j + 1, k - 10, i - 10, j + 4, k + 10, TowerDungeonBuildingBlocks.towerDungeonWallBlueUnbreakable);
            //+-Wall
            BlockFill.fillRectangle(world, i + 10, j + 1, k - 10, i - 10, j + 4, k - 10, TowerDungeonBuildingBlocks.towerDungeonWallBlueUnbreakable);
            //-+Wall
            BlockFill.fillRectangle(world, i - 10, j + 1, k + 10, i + 10, j + 4, k + 10, TowerDungeonBuildingBlocks.towerDungeonWallBlueUnbreakable);
            return true;
        }
        return false;
    }

    public static boolean genFloor_loot(World world, int i, int j, int k, String color) {
        genFloor_base(world, i, j, k, color);
        genLootChest(world, i + 9, j + 1, k, Blocks.chest);
        genLootChest(world, i - 9, j + 1, k, Blocks.chest);
        genLootChest(world, i, j + 1, k + 9, Blocks.chest);
        genLootChest(world, i, j + 1, k - 9, Blocks.chest);
        return true;
    }

    public static boolean genFloor_entrance(World world, int i, int j, int k, String color) {
        genFloor_base(world, i, j, k, color);
        BlockFill.fillRectangle(world, i - 1, j + 1, k - 10, i + 1, j + 3, k - 10, Blocks.air);
        BlockFill.fillRectangle(world, i - 1, j + 1, k + 10, i + 1, j + 3, k + 10, Blocks.air);
        BlockFill.fillRectangle(world, i - 10, j + 1, k - 1, i - 10, j + 3, k + 1, Blocks.air);
        BlockFill.fillRectangle(world, i + 10, j + 1, k - 1, i + 10, j + 3, k + 1, Blocks.air);
        return true;
    }

    public static void genLootChest(World world, int x, int y, int z, Block block) {
        Random random = new Random();
        world.setBlock(x, y, z, block);
        TileEntityChest chest = (TileEntityChest) world.getTileEntity(x, y, z);
        if (chest != null) {
            for (int i = 0; i < 8; i++) {
                chest.setInventorySlotContents(random.nextInt(28), LootItems.getRandomLoot());
            }
        }
    }
}